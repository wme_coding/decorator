import coffee.Americano;
import coffee.Cappuccino;
import coffee.Latte;
import extras.CoffeeWithMarshmallow;
import extras.CoffeeWithSugar;
import food.Cookie;
import food.Croissant;
import order.Order;
import orderpoc.OrderProcessor;
import org.junit.Assert;
import org.junit.Test;

import javax.security.auth.login.LoginException;

public class TestCaffe {
    public final double eps = 1e-10;
    OrderProcessor orderProcessor = new OrderProcessor();

    @Test
    public void testCaffeService() throws LoginException {
        Order firstOrder = new Order(
                new CoffeeWithSugar(0, new Latte(170)),
                    new Croissant(200));
        Assert.assertEquals(370, firstOrder.getOrderPrice(), eps);
        orderProcessor.addOrder(firstOrder);

        Order secondOrder = new Order(
                new Americano(100));
        Assert.assertEquals(100, secondOrder.getOrderPrice(), eps);
        orderProcessor.addOrder(secondOrder);

        Order thirdOrder = new Order(
                new CoffeeWithMarshmallow(50, new Cappuccino(190)),
                new Cookie(70));
        Assert.assertEquals(310, thirdOrder.getOrderPrice(), eps);
        orderProcessor.addOrder(thirdOrder);

        Assert.assertEquals("Cappuccino + Marshmallow, Cookie", orderProcessor.getCurrentOrderDescription());
        orderProcessor.completeOrder();
        Assert.assertEquals("Americano", orderProcessor.getCurrentOrderDescription());
        orderProcessor.completeOrder();
        Assert.assertEquals("Latte + Sugar, Croissant", orderProcessor.getCurrentOrderDescription());
        orderProcessor.completeOrder();

        Assert.assertEquals("No orders", orderProcessor.completeOrder());
    }
}
