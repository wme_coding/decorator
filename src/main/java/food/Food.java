package food;

import product.Product;

public abstract class Food implements Product {
    protected double price;
    protected String name;

    public double getProductPrice(){
        return price;
    }

    public String getProductName(){
        return name;
    }
}
