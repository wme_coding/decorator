package food;

public class Croissant extends Food{

    public Croissant(double price) {
        this.price = price;
        this.name = "Croissant";
    }
}
