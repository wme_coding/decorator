package extras;

import coffee.Coffee;
import product.Product;

public abstract class Extra implements Product {

    protected Coffee coffee;
    protected double price;
    protected String name;

    public String getProductName() {
        return coffee.getProductName() + " + " + name;
    }
}
