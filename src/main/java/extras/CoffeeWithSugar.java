package extras;

import coffee.Coffee;
import product.Product;

public class CoffeeWithSugar extends Extra{

    public CoffeeWithSugar(double price, Coffee coffee) {
        this.price = price;
        this.coffee = coffee;
        this.name = "Sugar";
    }

    @Override
    public double getProductPrice() {
        return this.price + coffee.getProductPrice();
    }
}
