package extras;

import coffee.Coffee;

public class CoffeeWithMarshmallow extends Extra {

    public CoffeeWithMarshmallow(double price, Coffee coffee) {
        this.price = price;
        this.coffee = coffee;
        this.name = "Marshmallow";
    }

    @Override
    public double getProductPrice() {
        return this.price + coffee.getProductPrice();
    }
}
