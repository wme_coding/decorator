package extras;

import coffee.Coffee;

public class CoffeeWithCinnamon extends Extra{

    public CoffeeWithCinnamon(double price, Coffee coffee) {
        this.price = price;
        this.coffee = coffee;
        this.name = "Cinnamon";
    }

    @Override
    public double getProductPrice() {
        return this.price + coffee.getProductPrice();
    }
}
