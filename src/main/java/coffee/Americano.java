package coffee;

public class Americano extends Coffee{

    public Americano(double price) {
        this.price = price;
        this.name = "Americano";
    }
}
