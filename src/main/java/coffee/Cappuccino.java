package coffee;

public class Cappuccino extends Coffee{

    public Cappuccino(double price) {
        this.price = price;
        this.name = "Cappuccino";
    }
}
