package coffee;

import product.Product;

public abstract class Coffee implements Product {

    protected double price;
    protected String name;

    public double getProductPrice(){
        return price;
    }

    public String getProductName(){
        return name;
    }
}
