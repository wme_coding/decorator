package coffee;

public class Espresso extends Coffee {

    public Espresso(double price) {
        this.price = price;
        this.name = "Espresso";
    }
}
