package coffee;

public class Latte extends Coffee{

    public Latte(double price) {
        this.price = price;
        this.name = "Latte";
    }
}
