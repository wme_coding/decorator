package product;

public interface Product {

    String getProductName();
    double getProductPrice();
}
