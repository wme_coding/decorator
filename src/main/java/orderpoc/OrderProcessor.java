package orderpoc;

import order.Order;
import product.Product;

import java.util.*;

public class OrderProcessor {

    Deque<Order> orders;

    public OrderProcessor(){
        orders = new ArrayDeque<>();
    }

    public void addOrder(Order order){
        orders.addFirst(order);
    }

    public String completeOrder(){
        if(orders.size() == 0){
            return "No orders";
        }
        orders.removeFirst();
        return "Completed";
    }

    public String getCurrentOrderDescription(){
        return orders.getFirst().getOrderDescription();
    }
}
