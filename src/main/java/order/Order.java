package order;

import product.Product;

import javax.security.auth.login.LoginException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Order {
    private List<Product> products;

    public Order(Product ... products) throws LoginException {
        if(products.length == 0){
            throw new LoginException();
        }
        this.products = new ArrayList<>();
        this.products.addAll(Arrays.asList(products));
    }

    public void addToOrder(Product product){
        products.add(product);
    }

    public double getOrderPrice(){
        double res = 0;
        for(Product p: products){
            res += p.getProductPrice();
        }
        return res;
    }

    public String getOrderDescription(){
        StringBuilder res = new StringBuilder();
        for(Product p: products){
            res.append(p.getProductName());
            res.append(", ");
        }
        return res.substring(0, res.length() - 2);
    }
}
